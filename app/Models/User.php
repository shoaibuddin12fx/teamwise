<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends \TCG\Voyager\Models\User
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function projects() {
        return $this->belongsToMany(Project::class, 'user_projects', 'user_id', 'project_id');
    }

    public function createdTasks() {
        return $this->hasMany(Task::class, 'created_by', 'id')->with(['links', 'project', 'user', 'user.trackerTime']);
    }

    public function assignedTasks() {
        return $this->hasMany(Task::class)->with(['links', 'project', 'user']);
    }

    public function trackerTime() {
        return $this->hasOne(TrackerTime::class);
    }

    public function projectPerHourRates(){
        return $this->belongsToMany(ProjectUserRate::class, 'user_id', 'id');
    }
}
