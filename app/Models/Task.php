<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        "user_id",
        "created_by",
        "project_id",
        "title",
        "due_date",
        "description",
        "priority",
        "time_limit",
        "created_at",
        "updated_at",
    ];

    protected $appends = ['fmtCreatedAt', 'fmtDueDate'];

    public function links(){
        return $this->hasMany(TaskLink::class);
    }

    public function project(){
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function creator(){
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function getFmtCreatedAtAttribute() {
        return Carbon::parse($this->created_at)->format('d M, Y');
    }

    public function getFmtDueDateAttribute() {
        return Carbon::parse($this->due_date)->format('d M, Y');
    }

    protected $casts = [
        'due_date'  => 'date:m/d/Y',
    ];

    public function delete()
    {
        $this->links()->delete();
        return parent::delete();
    }
}
