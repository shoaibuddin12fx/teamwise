<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectTimePause extends Model
{
    use HasFactory;

    protected $table = "project_time_pauses";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'tracker_time_id',
        'project_id',
        'record_time',
        'record_status',
        'record_time_difference'
    ];

}
