<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $table = "projects";

    public function categories(){
        return $this->belongsToMany(Category::class, 'project_categories', 'project_id', 'category_id');
    }

    public function userPerHourRates(){
        return $this->hasMany(ProjectUserRate::class, 'project_id', 'id');
    }

    public function tasks() {
        return $this->hasMany(Task::class, 'project_id', 'id');
    }
}
