<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Budget extends Model
{
    use HasFactory;

    protected $fillable = [ 'project_id', 'type', 'currency_id', 'amount' ];

    public function project() {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function currency() {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }

    public function budget_details() {
        return $this->hasMany(BudgetDetail::class, 'budget_id', 'id');
    }

    public function getFmtCreatedAtAttribute($value)
    {
        return Carbon::parse($this->created_at)->format('d M, Y');
    }
}
