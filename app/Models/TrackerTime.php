<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackerTime extends Model
{
    use HasFactory;

    protected $table = "tracker_times";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'is_timer_on',
        'is_stopped',
        'clock',
        'date'
    ];
}
