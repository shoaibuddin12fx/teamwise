<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class WaapsTimetracker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $header = $request->header();

        if(!isset($header['secretcode'])){
            $msgArray = array(
                'bool' => false,
                'status' => 403,
                "message" => 'Only Authorized persons are allowed to use tracker app'
            );

            $returnArray = array_merge($msgArray);
            return response()->json($returnArray, 403);
        }

        if(($header['secretcode'][0] != '055ee0968b40a0bf01004bcc3524b56cd64196b3177090bb7c00988d9b6a1037')){
            $msgArray = array(
                'bool' => false,
                'status' => 403,
                "message" => 'Only Authorized persons are allowed to use tracker app'
            );

            $returnArray = array_merge($msgArray);
            return response()->json($returnArray, 403);
        }else{
            return $next($request);
        }
        return $next($request);
    }
}
