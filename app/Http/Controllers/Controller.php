<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function success($msg, $data = [], $code = 200){

        $msgArray = array(
            'bool' => true,
            'status' => 200,
            "message" => $msg
        );

        $returnArray = array_merge($msgArray, $data);

        return response()->json( $returnArray, $code);
    }

    public static function failure($error, $data = [], $code = 422 ){

        $msgArray = array(
            'bool' => false,
            'status' => 403,
            "message" => $error
        );

        $returnArray = array_merge($msgArray, $data);

        return response()->json( $returnArray, $code);

    }

}
