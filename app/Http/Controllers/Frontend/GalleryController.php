<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Project;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    //
    public function index(Request $request)
    {

        $projects = Project::all();
        $categories = Category::all();



        return view('pages/gallery-details', compact($projects, $categories));
    }
}
