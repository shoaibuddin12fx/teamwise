<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\{Budget, BudgetDetail, Currency, Project};
use Illuminate\Http\Request;

class BudgetController extends Controller
{
    public function index()
    {
        return view('admin.budget.index')
            ->with('budgets', Budget::with(['project', 'currency', 'budget_details'])->paginate(20));
    }

    public function create()
    {
        return view('admin.budget.create')
            ->with('currencies', Currency::all())
            ->with('projects', Project::all());
    }


    public function store(Request $request)
    {
        $request->validate([
            'project_id' => 'required|unique:budgets',
            'type' => 'required',
            'amount' => 'required|integer|min:1',
            'start_date' => isset(request()->type) ? 'required|date' : '',
            'end_date' => isset(request()->type) ? 'required|date|after_or_equal:start_date' : '',
            'currency_id' => 'required',
        ],
        [
            'project_id.unique' => 'This project\'s budget has already created. Please update it or delete first to assign new one.',
        ]);

        $budget = new Budget();
        $budget->project_id = request()->project_id;
        $budget->type = request()->type;
        $budget->currency_id = request()->currency_id;
        $budget->amount = request()->amount;
        $budget->save();

        $this->saveOrUpdateBudgetDetails('store', request()->all(), $budget);

        return redirect()->route('custom.budgets.index')->with('message', 'New budget is successfully added against the project - ' . ucfirst($budget->project->name));

    }

    public function show($id)
    {
        //
    }

    public function edit(Budget $budget)
    {
        return view('admin.budget.edit')
            ->with('budget', $budget)
            ->with('currencies', Currency::all())
            ->with('projects', Project::all());
    }

    public function update(Budget $budget, Request $request)
    {
        $request->validate([
            'project_id' => 'required|unique:budgets,project_id,'.$budget->id,
            'type' => 'required',
            'amount' => 'required|integer|min:1',
            'start_date' => isset(request()->type) ? 'required|date' : '',
            'end_date' => isset(request()->type) ? 'required|date|after_or_equal:start_date' : '',
            'currency_id' => 'required',
        ],
        [
            'project_id.unique' => 'This project\'s budget has already created. Please update it or delete first to assign new one.',
        ]);

        $budget->project_id = request()->project_id;
        $budget->type = request()->type;
        $budget->currency_id = request()->currency_id;
        $budget->amount = request()->amount;
        $budget->save();

        $this->saveOrUpdateBudgetDetails('update', request()->all(), $budget);

        return redirect()->route('custom.budgets.index')->with('message', 'New budget is successfully updated against the project - ' . ucfirst($budget->project->name));
    }

    public function destroy($id)
    {
        //
    }

    public function saveOrUpdateBudgetDetails($action, $data, $budget) {
        if($action === 'store') {
            if(request()->type !== 'phase') {
                $budget->budget_details()->create([
                    'start_date' => request()->start_date,
                    'end_date' => request()->end_date
                ]);
            } else {
                $data = [];
                foreach(request()->start_date as $key => $stDate) {
                    $datum['budget_id'] = $budget->id;
                    $datum['start_date'] = $stDate;
                    $datum['end_date'] = request()->end_date[$key];
                    array_push($data, $datum);
                }
                BudgetDetail::insert($data);
            }
        } else {
            if(request()->type !== 'phase') {
                $budget->budget_details()->update([
                    'start_date' => request()->start_date,
                    'end_date' => request()->end_date
                ]);
            } else {
                $budget->budget_details()->delete();
                $data = [];
                foreach(request()->start_date as $key => $stDate) {
                    $datum['budget_id'] = $budget->id;
                    $datum['start_date'] = $stDate;
                    $datum['end_date'] = request()->end_date[$key];
                    array_push($data, $datum);
                }
                BudgetDetail::insert($data);
            }
        }
    }
}
