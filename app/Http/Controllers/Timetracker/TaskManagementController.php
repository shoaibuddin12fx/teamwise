<?php

namespace App\Http\Controllers\Timetracker;

use App\Http\Controllers\Controller;
use App\Repositories\Timetracker\TaskManagementRepository;
use App\Models\Task;
use App\Models\TaskLink;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;

class TaskManagementController extends Controller
{
    private $taskManageRepo;

    public function __construct(TaskManagementRepository $taskManageRepo) {
        $this->taskManageRepo = $taskManageRepo;
    }

    function getTaskList(){
        return self::success("Task List", [ 'data' => $this->taskManageRepo->getTaskList() ]);
    }

    function getAssignedTaskList(){
        return self::success("Task List", [ 'data' => $this->taskManageRepo->getAssignedTaskList() ]);
    }

    function addTask(Request $request){
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            //'user_id' => 'required',
            'project_id' => 'required',
            'title' => 'required',
            'due_date' => 'required',
            'time_limit' => 'required'
        ]);

        if ($validator->fails()) return self::failure($validator->errors()->first());

        $user = auth()->user();

        $obj = [
            "user_id" => $data['user_id'],
            "created_by" => $user->id,
            "project_id" => $data['project_id'],
            "title"  => $data['title'],
            "due_date"  => $data['due_date'],
            "description"  => $data['description'],
            "priority"  => $data['priority'],
            "time_limit"  => $data['time_limit'],
        ];

        $task = new Task();

        //$task->user_id = $data['user_id'];
        $task->user_id = $user->id;
        $task->created_by = $user->id;
        $task->project_id = $data['project_id'];
        $task->title  = $data['title'];
        $task->due_date = $data['due_date'];
        $task->description = $data['description'];
        $task->priority = $data['priority'];
        $task->time_limit = $data['time_limit'];

        $task->save();

        $linksCount = count($data['links']);

        $links = [];

        if($linksCount > 0){

            foreach($data['links'] as $link){
                $tasklink = new TaskLink();
                $tasklink->task_id = $task->id;
                $tasklink->link = $link;
                $tasklink->save();

                $links[] = $tasklink;
            }

        }

        $task["links"] = $links;

        return self::success("Task Created", [ "data" => $task ]);
    }

    function getTaskById(Task $task) {
        return self::success("Task Data", [ "data" => $this->taskManageRepo->getTaskById($task) ]);
    }

    function updateTask() {
        $data = request()->all();
        $user = auth()->user();

        $validator = Validator::make(request()->all(), [
            //'user_id' => 'required',
            'project_id' => 'required',
            'title' => 'required',
            'due_date' => 'required',
            'time_limit' => 'required'
        ]);

        if ($validator->fails()) return self::failure($validator->errors()->first());

        $task = Task::findOrFail(request()->id);
        //$task->user_id = $data['user_id'];
        $task->user_id = $user->id;
        $task->project_id = $data['project_id'];
        $task->title  = $data['title'];
        $task->due_date = $data['due_date'];
        $task->description = $data['description'];
        $task->priority = $data['priority'];
        $task->time_limit = $data['time_limit'];
        $task->save();

        if(count($data['deletedLinks']) > 0) TaskLink::whereIn('id', $data['deletedLinks'])->delete();

        $links = [];
        if(count($data['links']) > 0) {
            foreach($data['links'] as $link) {
                if(is_string($link)) {
                    $links[] = $task->links()->create([ 'link' => $link ]);
                    continue;
                }
                $tasklink = TaskLink::findOrFail($link['id']);
                $tasklink->link = $link['link'];
                $tasklink->save();
                $links[] = $tasklink;
            }
        }
        $task["links"] = $links;

        return self::success("Task Updated", ["data" => $task]);
    }

    function getProjectTaskList($id) {
        $userId = auth()->user()->id;
        return self::success("Project Task list", [ "data" => $this->taskManageRepo->getProjectTaskList($id, $userId) ]);
    }

    function deleteTaskById(Task $task) {
        return self::success("Task Deleted", [ "data" => $this->taskManageRepo->deleteTaskById($task) ]);
    }

    function updateTaskStatusById($data) {
        return self::success("Task Updated", [ "data" => $this->taskManageRepo->updateTaskStatusById($data) ]);
    }
}
