<?php

namespace App\Http\Controllers\Timetracker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\TrackerTime;
use App\Models\TrackerTimePause;
use Carbon\Carbon;
use App\Models\UserProject;
use App\Models\ProjectTimePause;
use App\Models\ProjectTime;

class TImeManagementController extends Controller
{
    //

    public function timercardUpdate(Request $request){

        $data = $request->all();
        $user = auth()->user();

        $validator = Validator::make($request->all(), [
            'is_timer_on' => 'required',
            'is_stopped' => 'required',
            'clock' => 'required',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        }


        $activeProject = UserProject::where(['user_id' => $user->id, 'active' => 1])->first();
        if(!$activeProject){
            return self::failure("PLease select your active project first");
        }



        // create a logic for start stop and pause times
        // if user doesn't have a timer of the day ... he will be able to start

        $dt = Carbon::now();
        $dateToday = $dt->toDateString();

        $tracker = TrackerTime::where('user_id', $user->id)->where('date', $dateToday )->first();

        if($tracker){

            $lastItem = TrackerTimePause::where('tracker_time_id', $tracker->id )->orderBy('id', 'desc')->first();
            if($lastItem && $lastItem->record_status == "stop"){
                return self::success("User Clock already stopped", ["data" => $tracker ] );
            }

        }

        if(!$tracker){
            $tracker = new TrackerTime();
            $tracker->user_id = $user->id;
            $tracker->date = $dateToday;
            $tracker->save();
        }

        $tracker->clock = $data["clock"];
        $tracker->is_timer_on = $data["is_timer_on"];
        $tracker->is_stopped = $data["is_stopped"];
        $tracker->save();

        $pauses = TrackerTimePause::where('tracker_time_id', $tracker->id )->get();

        $trackerPauses = new TrackerTimePause();
        $trackerPauses->tracker_time_id = $tracker->id;
        $trackerPauses->record_time = Carbon::now();


        $status = 'start';

        if($data["is_timer_on"] == true && $pauses->count() > 0){
            $status = 'resume';
        }

        if($data["is_timer_on"] == false && $pauses->count() > 0){
            $status = 'pause';
        }

        if($data["is_stopped"] == true || $data["is_stopped"] == 1){
            $status = 'stop';
        }

        if($pauses->count() > 0 && ( !($status == 'start') && !($status == 'resume'))){

            $firstTime = Carbon::parse($pauses[$pauses->count() - 1]["record_time"]);
            $secondTime = Carbon::now();
            $trackerPauses->record_time_difference = $secondTime->diff($firstTime)->format('%H:%I:%S');

        }

        $trackerPauses->record_status = $status;
        $trackerPauses->save();

        $clock = $this->getClockCalc($tracker->id);
        $tracker->clock = $clock;
        $tracker->save();
        $tracker->update([
            'clock' => $clock
        ]);


        $pm = new ProjectManagementController();
        $pm->pauseProjectSimultaneouslyWithTime($activeProject->project_id, $tracker->id);



        return self::success("User Clock updated", ["data" => $tracker, 'clock' => $clock ] );


    }

    public function getTimercardUpdate(Request $request){

        $user = auth()->user();

        $dt = Carbon::now();
        $dateToday = $dt->toDateString();
        $tracker = TrackerTime::where('user_id', $user->id)->where('date', $dateToday )->first();

        if(!$tracker){
            $tracker = new TrackerTime();
            $tracker->user_id = $user->id;
            $tracker->date = $dateToday;
            $tracker->clock = "00:00:00";
            $tracker->is_timer_on = false;
            $tracker->is_stopped = false;
            $tracker->save();
        }

        $clock = $this->getClockCalc($tracker->id);
        $tracker->clock = $clock;
        $tracker->save();
        $tracker->update([
            'clock' => $clock
        ]);

        return self::success("User Clock updated", ["data" =>  $tracker, 'clock' => $clock] );

    }


    public function getClockCalc($trackerId){

        $pauses = TrackerTimePause::where('tracker_time_id', $trackerId )->whereNotNull('record_time_difference')->get();

        $clock = "00:00:00";



        $slots = $pauses->map(function($item) {
            return [
                'record_time_difference' => $item['record_time_difference'],
            ];
        })->toArray();



        for($i = 0; $i < count($slots); $i++){

            $firstTime  = $slots[$i]["record_time_difference"];
            $secondTime = $clock;

            $clock = gmdate('H:i:s', strtotime($firstTime) + strtotime($secondTime));

        }


        $lastTime = TrackerTimePause::where('tracker_time_id', $trackerId )->orderBy('id', 'desc')->first();

        if($lastTime){
            if($lastTime->record_status == "resume" || $lastTime->record_status == "start"){

                $hmsLast = Carbon::parse($lastTime->record_time)->format('H:i:s'); //Carbon::parse($lastTime->record_time)->format('%H:%I:%S');
                $currentTime = Carbon::now()->format('H:i:s');

                $diffResume = gmdate('H:i:s', strtotime($currentTime) - strtotime($hmsLast));
                $clock = gmdate('H:i:s', strtotime($clock) + strtotime($diffResume));

            }
        }


        return $clock;

    }

    public function setTwoDigit($n) {
        return $n;//($n < 10) ? "0" . $n : $n;
    }










}
