<?php

namespace App\Http\Controllers\Timetracker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Http\Resources\Api\ApiUserResource;

class AuthController extends Controller
{
    public function loginInTImeTracker(Request $request){

        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8|max:16',
        ]);

        if ($validator->fails()) return self::failure($validator->errors()->first());

        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password])) return self::failure("Invalid credentials");

        $user = Auth::user();
        if ($user->id == 1) return self::failure("Super Admins are not allowed to use tracker app");

        $token = $user->createToken('WaapsTimetracker')->accessToken;
        $user = new ApiUserResource($user);
        $response = ['token' => $token, 'user' => $user];
        return self::success('login successful', ['data' => $response]);
    }

}
