<?php

namespace App\Http\Controllers\Timetracker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\{Project, ProjectTime, ProjectTimePause, TrackerTime, User, UserProject};

class ProjectManagementController extends Controller
{
    //

    public function getProjectcardUpdate(Request $request){

        $userId = auth()->user()->id;
        $userProjects = UserProject::where('user_id', $userId)->get();

        $projectIds = $userProjects->map(function($item){
            return $item->project_id;
        })->toArray();

        $projects = Project::whereIn('id', $projectIds)->get();

        $dt = Carbon::now();
        $dateToday = $dt->toDateString();

        $projects = $projects->transform(function ($item) use ($userProjects, $userId, $dateToday) {
            $usp = $userProjects->where('project_id', $item->id)->first();
            $projectTime = ProjectTime::where(['user_id' => $userId, 'project_id' => $item->id, 'date' => $dateToday])->first();
            $item["clock"] = $projectTime ? $projectTime->clock : "00:00:00";
            $item['active'] = $usp['active'];
            return $item;
        });

        return self::success("User Project List", ["data" => $projects ] );

    }

    public function setProjectcardProjectSelect(Request $request){

        $data = $request->all();
        $userId = auth()->user()->id;

        if(!isset($data['id'])) return self::failure("Project ID not provided");

        // $userProjects = UserProject::where(['user_id' => $userId, 'project_id' => $data['id']])->first();
        $userProjects = UserProject::with('project.tasks')->whereHas('project.tasks', function($q) use ($userId) {
            $q->where('created_by', $userId);
        })->where(['user_id' => $userId, 'project_id' => $data['id']])->first();

        // return self::success("Project Selection updated", ['data' => $userProjects ]);

        if(!$userProjects) return self::failure("Project not assigned to user");

        $currentActiveProjectId = UserProject::where(['user_id' => $userId, 'active' => 1 ])->orderBy('id', 'desc')->first();

        $toBeActiveProjectId = $data['id'];

        if(!empty($currentActiveProjectId)) $res = $this->switchActiveProjectAndDenoteTime($currentActiveProjectId->project_id, $toBeActiveProjectId)->getData();

        UserProject::where(['user_id' => $userId])->update([
            'active' => 0
        ]);

        UserProject::where(['user_id' => $userId, 'project_id' =>  $data['id']])->update([
            'active' => 1
        ]);

        $res = [];

        $data['active'] = 1;

        return self::success("Project Selection updated", ['data' => $data, 'res' => $res ]);
    }

    public function removeActiveProject(Request $request){

        $data = $request->all();

        $userId = auth()->user()->id;

        if(!isset($data['id'])){
            return self::failure("Project ID not provided");
        }

        $userProjects = UserProject::where(['user_id' => $userId, 'project_id' => $data['id']])->first();

        if(!$userProjects){
            return self::failure("Project not assigned to user");
        }

        $userProjects->update([
            'active' => 0
        ]);


        $res = $this->getProjectcardUpdate($request)->getData();

        return self::success("Project Selection updated", ['data' => $res->data]);

    }


    public function switchActiveProjectAndDenoteTime($currentActiveProjectId, $toBeActiveProjectId){

        // sudo logic will be
        // from the active project ... note down its tracker total time and record in the project time value of that day
        // then
        // from the switched project ... record the total time if previous time it has already ... record in total ... then start a clock again with zero zero

        $user = auth()->user();
        $dt = Carbon::now();
        $dateToday = $dt->toDateString();


        function setPauseStartProjectEntry($trackerId, $projectId, $record_status, $pclock ){

            $user = auth()->user();
            // tracker paused for that project
            $recordForProject = new ProjectTimePause();
            $recordForProject->tracker_time_id = $trackerId;
            $recordForProject->project_id = $projectId;
            $recordForProject->record_time = Carbon::now();
            $recordForProject->record_status = $record_status;
            $recordForProject->record_time_difference = $pclock;
            $recordForProject->save();


            // //
            // check for project time entry and if not .. add an entry there
            $dt = Carbon::now();
            $dateToday = $dt->toDateString();

            $projecttracker = ProjectTime::where(['user_id' => $user->id, 'project_id' => $projectId , 'date' => $dateToday])->first();

            if(!$projecttracker){
                $record_status = "start";

                $projecttracker = new ProjectTime();
                $projecttracker->user_id = $user->id;
                $projecttracker->project_id = $projectId;
                $projecttracker->date = $dateToday;
                $projecttracker->clock = $pclock;
                $projecttracker->status = $record_status;
                $projecttracker->save();
            }

            $projecttracker->clock = $pclock;
            $projecttracker->status = $record_status;
            $projecttracker->save();

            return [
                'recordForProject' => $recordForProject,
                'projecttracker' => $projecttracker
            ];

        }

        function toggleProjectStatusStartPause($projectId, $tracker){


            $ptp = ProjectTimePause::where(['project_id' => $projectId, 'tracker_time_id' => $tracker->id ])->get();

            $prd = null;
            $status = "start";
            $record_time = "00:00:00";

            if($ptp->count() > 0){

                $ptpArray = $ptp->toArray();
                $ptpLastRecord = $ptpArray[count($ptpArray) - 1];
                $ptpSecondLastRecord = isset($ptpArray[count($ptpArray) - 2]) ? $ptpArray[count($ptpArray) - 2] : null;

                $status = $ptpLastRecord["record_status"];

                switch($status){
                    case "start":

                        // find the last difference of time we have

                        $firstTime = Carbon::parse($ptpLastRecord["record_time"]);
                        $secondTime = Carbon::now();
                        $pclock = $secondTime->diff($firstTime)->format('%H:%I:%S');
                        $record_status = "pause";

                        $prd = setPauseStartProjectEntry($tracker->id, $projectId, $record_status, $pclock );

                    break;

                    case "pause":

                        // $firstTime = Carbon::parse($ptpLastRecord["record_time"]);
                        // $secondTime = Carbon::now();
                        $pclock = "00:00:00";//$secondTime->diff($firstTime)->format('%H:%I:%S');
                        $record_status = "start";

                        $prd = setPauseStartProjectEntry($tracker->id, $projectId, $record_status, $pclock );

                    break;
                    default:
                    break;
                }


            } else{

                $pclock = "00:00:00";
                $record_status = "start";

                $prd = setPauseStartProjectEntry($tracker->id, $projectId, $record_status, $pclock );

            }

            return [
                'top' => $prd
            ];

        }

        $tracker = TrackerTime::where('user_id', $user->id)->where('date', $dateToday )->first();

        $ptp = toggleProjectStatusStartPause($currentActiveProjectId, $tracker);
        $mtp = toggleProjectStatusStartPause($toBeActiveProjectId, $tracker);

        // if(!$mtp){
        //     // newly build has no tracker yet
        // }

        return self::success("records", ['data' => [
            'currentActiveProjectId' => $currentActiveProjectId,
            'toBeActiveProjectId' => $toBeActiveProjectId
        ], 'ptp' => $ptp, 'mtp' => $mtp ]);

    }

    public function pauseProjectSimultaneouslyWithTime($currentActiveProjectId, $trackerId){

        // update clock for active project as well

        $user = auth()->user();

        $ptp = ProjectTimePause::where(['project_id' => $currentActiveProjectId, 'tracker_time_id' => $trackerId ])->orderBy('id', 'desc')->first();

        $pclock = "00:00:00";
        $record_time = Carbon::now();
        $record_status = "start";


        if($ptp){

            if($ptp->record_status == "start" || $ptp->record_status == "resume"){
                $firstTime = Carbon::parse($ptp->record_time);
                $secondTime = Carbon::now();
                $pclock = $secondTime->diff($firstTime)->format('%H:%I:%S');
            }

        }


        $recordForProject = new ProjectTimePause();
        $recordForProject->tracker_time_id = $trackerId;
        $recordForProject->project_id = $currentActiveProjectId;
        $recordForProject->record_time = $record_time;
        $recordForProject->record_status = $record_status;
        $recordForProject->record_time_difference = $pclock;
        $recordForProject->save();


        // check for project time entry and if not .. add an entry there
        $dt = Carbon::now();
        $dateToday = $dt->toDateString();

        $projecttracker = ProjectTime::where(['user_id' => $user->id, 'project_id' => $currentActiveProjectId , 'date' => $dateToday])->first();

        if(!$projecttracker){
            $projecttracker = new ProjectTime();
            $projecttracker->user_id = $user->id;
            $projecttracker->project_id = $currentActiveProjectId;
            $projecttracker->date = $dateToday;
            $projecttracker->clock = "00:00:00";
            $projecttracker->status = $record_status;
            $projecttracker->save();
        }

        $projecttracker->clock = $pclock;
        $projecttracker->status = $record_status;
        $projecttracker->save();






        return self::success("records", ['data' => null ]);



    }

    public function userAssignedProject(){

        $userId = auth()->user()->id;
        $userProjects = UserProject::where('user_id', $userId)->get();

        $projectIds = $userProjects->map(function($item){
            return $item->project_id;
        })->toArray();

        $projects = Project::whereIn('id', $projectIds)->get();

        return self::success("User Project List", ["data" => $projects ] );
    }

    public function getEmployees() {
        $userId = auth()->user()->id;

        $employees = User::where('role_id', 3)->get();
        $employees = $employees->transform(function($item){
            $item['avatar'] = 'http://127.0.0.1:8000/images/users/default.png';
            return $item;
        });

        return self::success("User Project List", ["data" => $employees ] );
    }

    public function getProjects() {
        $projects = Project::with('categories', 'userPerHourRates')->get();
        return self::success("Projects", ["data" => $projects ] );
    }
}
