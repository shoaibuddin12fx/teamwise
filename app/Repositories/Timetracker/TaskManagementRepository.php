<?php

namespace App\Repositories\Timetracker;

use App\Models\Task;

class TaskManagementRepository
{
    function getTaskList() {
        return auth()->user()->createdTasks()->get();
    }

    function getAssignedTaskList() {
        return auth()->user()->assignedTasks()->get();
    }

    function getTaskById($task) {
        return $task->load('project', 'user', 'links');
    }

    function deleteTaskById($task) {
        return $task->delete();
    }

    function getProjectTaskList($id, $userId) {
        return Task::where('project_id', $id)->where('user_id', $userId)->get();
    }

    function updateTaskStatusById($data) {
        return $data;
    }
}
