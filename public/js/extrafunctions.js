jQuery(function () {

    $('#call-directions').click( function (){

        // get users current location first

        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(function (position) {
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                window.open(`https://www.google.com/maps?saddr=24.908996639627233,67.1041068397777&daddr=${lat},${lng}`,'_blank');
            });
        } else {
            // browser not supported geolocation api so just show regular map location
            window.open(`https://www.google.com/maps?q=24.908996639627233,67.1041068397777`,'_blank');
        }


    });


});
