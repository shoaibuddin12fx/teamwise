<?php

use TImetracker\TaskManagementController;
use TImetracker\TImeManagementController;
use TImetracker\ProjectManagementController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('postcontact', 'Frontend\HomeController@postcontact');

Route::group(['prefix' => 'timetracker', 'middleware' => 'WaapsTimetracker'], function () {

    Route::post('/login', 'TImetracker\AuthController@loginInTImeTracker');

    Route::group(['middleware' => 'auth:api'], function(){

        // Route::get('logged-user', function () {
        //     return auth()->user();
        // });

        // Route::post('logout', function () {
        //     Auth::logout();
        // });

        Route::controller(ProjectManagementController::class)
            ->group(function () {
                Route::get('/projectcard-update', 'getProjectcardUpdate');
                Route::post('/projectcard-project-selected', 'setProjectcardProjectSelect');
                Route::post('/projectcard-project-remove-active', 'removeActiveProject');
                Route::get('user-assigned-project',  'userAssignedProject');
                Route::get('get-projects',  'getProjects');
                Route::get('get-employees',  'getEmployees');
                Route::get('switch-active-project-and-denote-time/{currentActiveProjectId}/{toBeActiveProjectId}',  'switchActiveProjectAndDenoteTime');
            });

        // *** timer cards update *** //
        Route::controller(TImeManagementController::class)->group(function () {
            Route::post('/timercard-update', 'timercardUpdate');
            Route::get('/timercard-update', 'getTimercardUpdate');
        });

        // *** task crud *** //
        Route::controller(TaskManagementController::class)->group(function() {
            Route::post('add-task', 'addTask');
            Route::get('task-list', 'getTaskList');
            Route::get('assigned-task-list', 'getAssignedTaskList');
            Route::get('project-wise-task-list/{id}', 'getProjectTaskList');
            Route::get('update-task-status-by-id/{data}', 'updateTaskStatusById');
            Route::get('get-task-by-id/{task}', 'getTaskById');
            Route::get('delete-task-by-id/{task}', 'deleteTaskById');
            Route::post('update-task', 'updateTask');
        });

    });

});

Route::get('login', 'Frontend\HomeController@postcontact');
