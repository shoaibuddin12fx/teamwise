<?php

use Illuminate\Support\Facades\Route;
use Admin\BudgetController;


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::prefix('custom')
        ->name('custom.')
        ->group(function() {
            Route::resource('budgets', BudgetController::class);
        });
});


Route::get('/', 'Frontend\HomeController@index');
Route::get('/blogs', 'Frontend\BlogController@index');
Route::get('/gallery-detail','Frontend\GalleryController@index');
