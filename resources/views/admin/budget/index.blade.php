@include('admin.includes.header')

<div class="container mt-5">
    @if (session()->has('message'))
        <div class="alert alert-success round shadow">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-end mb-3">
                <a href="{{ route('custom.budgets.create') }}" class="btn btn-secondary shadow round">Add New Budget</a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card round shadow">
                <div class="card-header">
                    <h5 class="card-title">Budget Listing</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Project</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($budgets as $budget)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $budget->fmt_created_at }}</td>
                                        <td>{{ $budget->project->name }}</td>
                                        <td>{{ ucfirst($budget->type) }}</td>
                                        <td>{{ $budget->currency->symbol }}{{ $budget->amount }}</td>
                                        <td>
                                            <a href="{{ route('custom.budgets.show', $budget->id) }}"
                                                class="text-secondary text-decoration-none">View</a> |
                                            <a href="{{ route('custom.budgets.edit', $budget->id) }}"
                                                class="text-secondary text-decoration-none">Edit</a> |
                                            <a href="javascript:void(0)"
                                                class="text-secondary text-decoration-none">Delete</a> |
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <th colspan="7" class="text-center">No record found.</th>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                @if ($budgets->count() > 0)
                    <div class="card-footer">
                        {{ $budgets->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
