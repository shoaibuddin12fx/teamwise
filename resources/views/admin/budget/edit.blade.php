@include('admin.includes.header')

<div class="container mt-5">
    @if (count($errors) > 0)
        <div class="alert alert-danger shadow round">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-end mb-3">
                <a href="{{ route('custom.budgets.index') }}" class="btn btn-secondary shadow round">Back</a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow round">
                <div class="card-header round">
                    <h5 class="card-title">Update Budget</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('custom.budgets.update', $budget->id) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="project_id">Project</label>
                                    <select id="project_id" name="project_id" class="form-control round">
                                        <option disabled selected>Choose an option</option>
                                        @if (count($projects) > 0)
                                            @foreach ($projects as $project)
                                                <option value="{{ $project->id }}"
                                                    {{ $budget->project_id === $project->id ? 'selected' : '' }}>
                                                    {{ $project->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 type-div">
                                <div class="form-group">
                                    <label for="type">Type</label>
                                    <select id="type" name="type" class="form-control round">
                                        <option disabled selected>Choose an option</option>
                                        <option value="hourly" {{ $budget->type === 'hourly' ? 'selected' : '' }}>
                                            Hourly
                                        </option>
                                        <option value="monthly" {{ $budget->type === 'monthly' ? 'selected' : '' }}>
                                            Monthly</option>
                                        <option value="phase" {{ $budget->type === 'phase' ? 'selected' : '' }}>Phase
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="for-edit-purpose">
                            @if ($budget->type !== 'phase')
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="start_date">Start Date</label>
                                            <input type="date" name="start_date"class="form-control round">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="end_date">End Date</label>
                                            <input type="date" name="end_date" class="form-control round">
                                        </div>
                                    </div>
                                </div>
                            @else
                                <h6 class="text-secondary">Phase</h6>
                                @for ($i = 0; $i < $budget->budget_details()->count(); $i++)
                                    <div class="phase-div">
                                        <ul class="list-group mb-3 round shadow">
                                            <li class="list-group-item">
                                                <div class="row mt-2">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <input type="date" name="start_date[]"
                                                                class="form-control round start_date">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <input type="date" name="end_date[]"
                                                                class="form-control round end_date">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 add-btn-div {{ $i === 0 ? '' : 'd-none' }}">
                                                        <div class="form-group">
                                                            <button type="button"
                                                                class="btn btn-md btn-secondary shadow round add-btn">+</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 remove-btn {{ $i !== 0 ? '' : 'd-none' }}">
                                                        <div class="form-group">
                                                            <button type="button"
                                                                class="btn btn-md btn-danger shadow round">-</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                @endfor
                            @endif
                        </div>
                        <div id="static-field"></div>
                        <div id="dynamic-field"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="currency_id">Currency</label>
                                    <select id="currency_id" name="currency_id" class="form-control round">
                                        <option disabled selected>Choose an option</option>
                                        @if (count($currencies) > 0)
                                            @foreach ($currencies as $currency)
                                                <option value="{{ $currency->id }}"
                                                    {{ $budget->currency_id === $currency->id ? 'selected' : '' }}>
                                                    {{ $currency->name }}
                                                    ({{ $currency->symbol }})
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="amount">Amount</label>
                                    <input type="number" name="amount" class="form-control round" id="amount"
                                        value="{{ $budget->amount }}">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-center">
                            <button type="submit" class="btn btn-secondary shadow round">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.includes.footer')
<script>
    $(document).on('change', '[name="type"]', function() {
        $('#dynamic-field, #static-field, .for-edit-purpose').empty();
        if ($(this).val() === 'phase') {
            $('#dynamic-field').append(
                `<h6 class="text-secondary">Phase</h6><div class="phase-div">
                <ul class="list-group mb-3 round shadow"><li class="list-group-item"><div class="row mt-2">
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="date" name="start_date[]" class="form-control round start_date">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="date" name="end_date[]" class="form-control round end_date">
                        </div>
                    </div>
                    <div class="col-md-2 add-btn-div">
                        <div class="form-group">
                            <button type="button" class="btn btn-md btn-secondary shadow round add-btn">+</button>
                        </div>
                    </div>
                    <div class="col-md-2 remove-btn d-none">
                        <div class="form-group">
                            <button type="button"
                                class="btn btn-md btn-danger shadow round">-</button>
                        </div>
                    </div>
                </div></li></ul></div>`
            );

        } else {
            $('#static-field').append(
                `<div class="row default-dates-div">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="date" name="start_date"class="form-control round" id="start_date">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="date" name="end_date" class="form-control round" id="end_date">
                    </div>
                </div>
            </div>`
            );
        }

    });

    $(document).on("click", '.add-btn', function() {
        let phaseDiv = $('.phase-div').eq(0).clone();
        phaseDiv = phaseDiv.find('.add-btn-div').remove().end();
        phaseDiv.find('.remove-btn').removeClass('d-none');
        const _input = phaseDiv.find('input');
        _input.attr({
            required: true
        }).val('');
        $('#dynamic-field').append(phaseDiv);
        _input.focus();
    });

    $(document).on('click', '.remove-btn button', () => $(event.target).parents('div.phase-div').remove()
        .end());
</script>
