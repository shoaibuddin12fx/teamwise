@extends('layouts.default')
@section('content')

    <!--page Header-->
    <section class="page-header parallaxie padding_top center-block">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-titles text-center">
                        <h2 class="whitecolor font-light bottom30">latest Blog</h2>
                        <ul class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">latest Blog</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--page Header ends-->


    <!-- Our Blogs -->
    <section id="our-blog" class="bglight padding text-center">
        <div class="container">
            <div id="blog-measonry" class="cbp">
                <div class="cbp-item">
                    <div class="news_item shadow">
                        <a class="image" href="blog-detail.html">
                            <img src="images/blog-measonry1.jpg" alt="Latest News" class="img-responsive">
                        </a>
                        <div class="news_desc">
                            <h3 class="text-capitalize font-light darkcolor"><a href="blog-detail.html">Next Large Social Network</a></h3>
                            <ul class="meta-tags top20 bottom20">
                                <li><a href="#."><i class="fa fa-calendar"></i>Feb 14</a></li>
                                <li><a href="#."> <i class="fa fa-user-o"></i> peter </a></li>
                                <li><a href="#."><i class="fa fa-comment-o"></i>5</a></li>
                            </ul>
                            <p class="bottom35">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <a href="blog-detail.html" class="button btnprimary btn-gradient-hvr">Read more</a>
                        </div>
                    </div>
                </div>
                <div class="cbp-item">
                    <div class="news_item shadow">
                        <a class="image" href="blog-detail.html">
                            <img src="images/blog-measonry2.jpg" alt="Latest News" class="img-responsive">
                        </a>
                        <div class="news_desc">
                            <h3 class="text-capitalize font-light darkcolor"><a href="blog-detail.html">Next Large Social Network</a></h3>
                            <ul class="meta-tags top20 bottom20">
                                <li><a href="#."><i class="fa fa-calendar"></i>Feb 16</a></li>
                                <li><a href="#."> <i class="fa fa-user-o"></i> </a></li>
                                <li><a href="#."><i class="fa fa-comment-o"></i>5 </a></li>
                            </ul>
                            <p class="bottom35">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                            <a href="blog-detail.html" class="button btnsecondary">Read more</a>
                        </div>
                    </div>
                </div>
                <div class="cbp-item">
                    <div class="news_item shadow">
                        <a class="image" href="blog-detail.html">
                            <img src="images/blog-measonry3.jpg" alt="Latest News" class="img-responsive">
                        </a>
                        <div class="news_desc">
                            <h3 class="text-capitalize font-light darkcolor"><a href="blog-detail.html">Next Large Social Network</a></h3>
                            <ul class="meta-tags top20 bottom20">
                                <li><a href="#."><i class="fa fa-calendar"></i>Feb 28</a></li>
                                <li><a href="#."> <i class="fa fa-user-o"></i> peter </a></li>
                                <li><a href="#."><i class="fa fa-comment-o"></i>5 </a></li>
                            </ul>
                            <p class="bottom35">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
                            <a href="blog-detail.html" class="button btnprimary btn-gradient-hvr">Read more</a>
                        </div>
                    </div>
                </div>
                <div class="cbp-item">
                    <div class="news_item shadow">
                        <a class="image" href="blog-detail.html">
                            <img src="images/blog-measonry5.jpg" alt="Latest News" class="img-responsive">
                        </a>
                        <div class="news_desc">
                            <h3 class="text-capitalize font-light darkcolor"><a href="blog-detail.html">Next Large Social Network</a></h3>
                            <ul class="meta-tags top20 bottom20">
                                <li><a href="#."><i class="fa fa-calendar"></i>Apr 14</a></li>
                                <li><a href="#."> <i class="fa fa-user-o"></i> peter </a></li>
                                <li><a href="#."><i class="fa fa-comment-o"></i>5 </a></li>
                            </ul>
                            <p class="bottom35">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                            <a href="blog-detail.html" class="button btnsecondary">Read more</a>
                        </div>
                    </div>
                </div>
                <div class="cbp-item">
                    <div class="news_item shadow">
                        <a class="image" href="blog-detail.html">
                            <img src="images/blog-measonry4.jpg" alt="Latest News" class="img-responsive">
                        </a>
                        <div class="news_desc">
                            <h3 class="text-capitalize font-light darkcolor"><a href="blog-detail.html">Next Large Social Network</a></h3>
                            <ul class="meta-tags top20 bottom20">
                                <li><a href="#."><i class="fa fa-calendar"></i>Mar 14</a></li>
                                <li><a href="#."> <i class="fa fa-user-o"></i> peter </a></li>
                                <li><a href="#."><i class="fa fa-comment-o"></i>5 </a></li>
                            </ul>
                            <p class="bottom35">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                            <a href="blog-detail.html" class="button btnprimary btn-gradient-hvr">Read more</a>
                        </div>
                    </div>
                </div>
                <div class="cbp-item">
                    <div class="news_item shadow">
                        <a class="image" href="blog-detail.html">
                            <img src="images/blog-measonry6.jpg" alt="Latest News" class="img-responsive">
                        </a>
                        <div class="news_desc">
                            <h3 class="text-capitalize font-light darkcolor"><a href="blog-detail.html">Next Large Social Network</a></h3>
                            <ul class="meta-tags top20 bottom20">
                                <li><a href="#."><i class="fa fa-calendar"></i>Jan 14</a></li>
                                <li><a href="#."> <i class="fa fa-user-o"></i> peter </a></li>
                                <li><a href="#."><i class="fa fa-comment-o"></i>5 </a></li>
                            </ul>
                            <p class="bottom35">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <a href="blog-detail.html" class="button btnsecondary">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <!--Pagination-->
                    <ul class="pagination justify-content-center top50">
                        <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--Our Blogs Ends-->
@stop
