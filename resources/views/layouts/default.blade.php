<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head')
</head>
<body data-bs-spy="scroll" data-bs-target="#xenav">
@include('includes.header')

@yield('content')

<footer id="site-footer" class="padding_half">
    @include('includes.footer')
</footer>

@include('includes.footerscripts')

</div>
</body>
</html>
