<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Waapsdeveloper</title>
<link href="images/favicon.jpeg" rel="icon">

<link rel="stylesheet" href="css/plugins.css">
<link rel="stylesheet" href="css/style.css">
<link href='//cdn.jsdelivr.net/npm/devicons@1.8.0/css/devicons.min.css' rel='stylesheet'>
