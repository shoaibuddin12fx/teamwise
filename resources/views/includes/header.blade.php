<!--PreLoader-->
<div class="loader">
    <div class="loader-inner">
        <div class="loader-blocks">
            <span class="block-1"></span>
            <span class="block-2"></span>
            <span class="block-3"></span>
            <span class="block-4"></span>
            <span class="block-5"></span>
            <span class="block-6"></span>
            <span class="block-7"></span>
            <span class="block-8"></span>
            <span class="block-9"></span>
            <span class="block-10"></span>
            <span class="block-11"></span>
            <span class="block-12"></span>
            <span class="block-13"></span>
            <span class="block-14"></span>
            <span class="block-15"></span>
            <span class="block-16"></span>
        </div>
    </div>
</div>
<!--PreLoader Ends-->

<!-- header -->
<header class="site-header">
    <nav class="navbar navbar-expand-lg center-brand static-nav">
        <div class="container">
            <a class="navbar-brand" href="">
                {{--                <img src="images/logo-transparent.png" alt="logo" class="logo-default"> --}}
                {{--                <img src="images/logo-dark.png" alt="logo" class="logo-scrolled"> --}}
                <img src="images/logo-sm.png" alt="logo" class="logo-default" style="margin-top: 50px">
                <img src="images/logo-sc.png" alt="logo" class="logo-scrolled">
            </a>
            <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-bs-toggle="collapse"
                data-bs-target="#xenav">
                <span> </span>
                <span> </span>
                <span> </span>
            </button>
            <div class="collapse navbar-collapse" id="xenav">
                <ul class="navbar-nav" id="container">
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#banner-main">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#our-process">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#our-team">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#portfolio_top">Portfolio</a>
                    </li>
                </ul>
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#our-pricings">Packages</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#our-testimonial">Clients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#our-blog">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#contactus">contactt </a>
                    </li>
                </ul>
            </div>
        </div>

        <!--side menu open button-->
        <a href="javascript:void(0)" class="d-none d-lg-inline-block sidemenu_btn" id="sidemenu_toggle">
            <span></span> <span></span> <span></span>
        </a>
    </nav>

    <!-- side menu -->
    <div class="side-menu">
        <div class="inner-wrapper">
            <span class="btn-close" id="btn_sideNavClose"><i></i><i></i></span>
            <nav class="side-nav w-100">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#banner-main">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#our-process">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#our-team">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#portfolio_top">Portfolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#our-pricings">Packages</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#our-testimonial">Clients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#our-blog">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pagescroll" href="#contactus">contact</a>
                    </li>
                </ul>
            </nav>

            <div class="side-footer w-100">
                <ul class="social-icons-simple white top40">
                    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
                    <li><a href="javascript:void(0)"><i class="fa fa-instagram"></i> </a> </li>
                    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
                </ul>
                <p class="whitecolor">&copy; 2021 XeOne. Made With Love by themesindustry</p>
            </div>
        </div>
    </div>
    <a id="close_side_menu" href="javascript:void(0);"></a>
    <!-- End side menu -->
</header>
<!-- header -->
<?php
