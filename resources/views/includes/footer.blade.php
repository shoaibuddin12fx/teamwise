    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <ul class="social-icons bottom25 wow fadeInUp" data-wow-delay="300ms">
                    <li><a href="https://www.facebook.com/waapsdeveloper" target="_blank"><i class="fa fa-facebook"></i> </a> </li>
{{--                    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>--}}
{{--                    <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i> </a> </li>--}}
                    <li><a href="https://www.linkedin.com/company/waapsdeveloper" target="_blank"><i class="fa fa-linkedin"></i> </a> </li>
                    <li><a href="https://www.instagram.com/waaps_developer/" target="_blank"><i class="fa fa-instagram"></i> </a> </li>
                    <li><a href="mailto:waapsdeveloper.com"><i class="fa fa-envelope-o"></i> </a> </li>
                </ul>
                <p class="copyrights wow fadeInUp" data-wow-delay="350ms"> &copy; 2022 Waapsdeveloper. All rights reserved  </p>
            </div>
        </div>
    </div>




<?php
